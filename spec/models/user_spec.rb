require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user = FactoryBot.build(:user)
  end

  describe 'factory data from FactoryBot' do
    it 'is valid when created from factory' do
      expect( @user ).to be_valid
    end
  end
  
  describe 'before_save -downcase-' do
    it 'メアド保存時に小文字に変換されること' do
      mixed_case_email = 'address@ExaMplE.CoM'
      @user.email = mixed_case_email
      @user.save
      expect( @user.email ).to eq mixed_case_email.downcase
    end
  end
  
  describe 'validates :name' do
    it 'is invalid when name is nil' do
      @user.name = nil
      expect( @user ).not_to be_valid
    end
    it 'is invalid when name.length > 64' do
      @user.name = 'a'*65
      expect( @user ).not_to be_valid
    end
  end
  
  describe 'validates :email' do
    describe '-format-' do
      let(:address){
        [
          'user@example,com',
          'user_at_foo.org',
          'user.name@example.',
          'foo@bar_baz.com',
          'foo@bar+baz.com'
        ]
      }
      it 'is invalid when format is NG' do
        address.each do |add|
          @user.email = add
          expect( @user ).not_to be_valid
        end
      end
    end

    describe '-uniqueness-' do
      it 'is invalid when duplicate address' do
        user = FactoryBot.create(:user)
        duplicate_user = user.dup
        expect( duplicate_user ).not_to be_valid
      end
    end

    # 大文字、小文字を区別しないテスト
    describe '-case_sensitive-' do
      it 'is invalid when 大文字、小文字の区別しかないとき' do
        user = FactoryBot.create(:user)
        duplicate_user = user.dup
        duplicate_user.email = user.email.upcase
        expect( duplicate_user ).not_to be_valid
      end
    end

    describe 'others' do
      it 'is invalid when email is nil' do
        @user.email = nil
        expect( @user ).not_to be_valid
      end
      it 'is invalid when email.length > 255' do
        @user.email = 'a'*256
        expect( @user ).not_to be_valid
      end
    end
  end

  describe 'validates :password_digest' do
    it 'is invalid when password is nil' do
      @user.password = ' '*8
      @user.password_confirmation = ' '*8
      expect( @user ).not_to be_valid
    end

    it 'is invalid when password_length < 8' do
      @user.password = '1234abc'
      @user.password_confirmation = '1234abc'
      expect( @user ).not_to be_valid
    end
  end
end
