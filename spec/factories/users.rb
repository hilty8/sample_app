FactoryBot.define do
  factory :user do
    name { "name1" }
    email { "address@example.com" }
    password { "1234abcD"}
    password_confirmation { "1234abcD" }
  end
end
