require 'rails_helper'

RSpec.feature "Users", type: :feature do
  scenario "signup with wrong-parameter" do
    # アクセス＋登録データ入力
    visit signup_path
    fill_in "Name", with: ""
    fill_in "Email", with: "a@example"
    fill_in "Password", with: "foo"
    fill_in "Confirmation", with: "bar"
    click_on "Create my account"
    
    # エラーメッセージが表示されていることを確認
    expect(page).to have_content "Name can't be blank"
    expect(page).to have_content "Email is invalid"
    expect(page).to have_content "Password confirmation doesn't match Password"
    expect(page).to have_content "Password is too short"
  end
end