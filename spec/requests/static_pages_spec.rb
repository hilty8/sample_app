require 'rails_helper'

RSpec.describe StaticPagesController, type: :request do
  it 'returns #show' do
    #get '/static_pages/home' # 書き方を理解してなかったミス
    get root_path
    expect(response).to render_template(:home)
  end
end