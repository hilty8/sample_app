class User < ApplicationRecord
  # DBに保存する際に小文字に強制変換
    #before_save { self.email = email.downcase }も可能
  before_save { email.downcase! }

  validates :name, presence: true, length: { maximum: 64 }

  # email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
    uniqueness: { case_sensitive: false },
    format: { with: VALID_EMAIL_REGEX }
  
  # 
  has_secure_password validations: true
  validates :password, presence: true, length: { minimum: 8 }
end
